<?php
/**
 * @file
 * Include file to tell Views where our view export is located.
 */

/**
 * Implements hook_views_default_views().
 */
function simple_image_gallery_views_default_views() {
  // Declare all the .view files in the views subdir that end in .view
  $files = file_scan_directory(drupal_get_path('module', 'simple_image_gallery') . '/views', '/.view/');
  foreach ($files as $absolute => $file) {
    require $absolute;
    if (isset($view)) {
      $views[$file->name] = $view;
    }
  }
  return $views;
}
