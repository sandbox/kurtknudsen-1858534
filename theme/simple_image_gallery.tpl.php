<?php

/**
 * @file
 * Primary template for the image gallery.
 */
?>

<div class="simple_image_gallery-wrapper">
  <div class="simple_image_gallery-browser-wrapper">
    <div class="simple_image_gallery-browser">
      <?php echo $thumbnails ?>
    </div>
  </div>
  <div class="simple_image_gallery-viewer-wrapper">
    <div id="simple_image_gallery-viewer" class="simple_image_gallery-viewer">
      Click an image to see a larger version.
    </div>
  </div>
</div>
