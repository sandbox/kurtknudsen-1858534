<?php

/**
 * @file
 * Template file for the thumbnails.
 */
?>

<?php $img_link = image_style_url('simple_image_gallery_thumb', $image['thumb']); ?>
<a href="<?php echo $image['src'] ?>"><img
  id="simple_image_gallery_tn-<?php echo $image['id'] ?>"
  src="<?php print $img_link; ?>"
  /></a>
