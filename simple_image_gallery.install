<?php

/**
 * @file
 * Install, update, and uninstall functions for the simple_image_gallery module.
 */

/**
 * Implements hook_install().
 *
 * @ingroup simple_image_gallery
 */
function simple_image_gallery_install() {
  // Set a variable containing the name of the style to use when the module
  // outputs an image.
  variable_set('simple_image_gallery_style_name', 'simple_image_gallery_style');
}

/**
 * Implements hook_uninstall().
 *
 * @ingroup simple_image_gallery
 */
function simple_image_gallery_uninstall() {
  variable_del('simple_image_gallery_style_name');
  variable_del('simple_image_gallery_image_fid');
}

/**
 * Implements hook_enable().
 *
 * @ingroup simple_image_gallery
 */
function simple_image_gallery_enable() {
  // There is currently no way to manually flush an image style which causes
  // problems when installing a new module that implements
  // hook_image_styles_alter(). If the new module modifies an image style that
  // modification will not be applied to any images that have already been
  // generated unless the styles are flushed. This is one way around that.
  $styles = image_styles();
  foreach ($styles as $style) {
    image_style_flush($style);
  }
}

/**
 * Implements hook_disable().
 *
 * @ingroup simple_image_gallery
 */
function simple_image_gallery_disable() {
  // Solves the same problem as image_example_enable().
  simple_image_gallery_enable();
}
