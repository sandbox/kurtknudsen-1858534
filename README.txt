Simple Image Gallery
--------------------

This is my first module since I've been teaching myself Drupal 7.
I've only been learning for a few weeks now and have been working on this
on and off for about 2 weeks.

Some of the code is a bit redundant, but I wanted to learn (and share) about
using custom Image Styles and setting Image Links to fields. I also used
Views to create a custom page to view the same content as you would if you
went directly to the node ID. This really isn't meant to be used in a
production environment, but more as a learning module to understand various
concepts.

Feel free to do as you wish with the code and module.
