(function($) {

  Drupal.behaviors.simple_image_gallery = {
    attach: function(context) {
      $('.simple_image_gallery-browser-wrapper', context).find('.simple_image_gallery-browser a').live('click', function() {
        //Get the img source.
        var img_src = $(this).attr('href');
        //Simply replace the div's content with a simple IMG tag.
        $('div#simple_image_gallery-viewer', context).html("<img src='" + img_src + "' />");
        //Return false so it stops processing the click.
        //This will prevent the page from jumping to the top each time you click an image.
        return false;
      });
    }
  };

})(jQuery);
